package com.example.sfuevents.data.room.tokens

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

private const val TOKEN_DB_NAME = "token.db"

@Database(entities = [TokenEntity::class], version = 1)
abstract class TokenDatabase : RoomDatabase() {
    abstract fun tokenDao(): TokenDao

    companion object {
        @Volatile
        private var INSTANCE: TokenDatabase? = null

        /**
         * Get singleton for database
         */
        @Synchronized
        fun getDatabase(context: Context): TokenDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context, TokenDatabase::class.java, TOKEN_DB_NAME).build()
            }
            return INSTANCE!!
        }
    }
}