package com.example.sfuevents.data

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class EventViewModel : ViewModel() {
    var events = MutableLiveData<List<EventEntity>>()

    var currentDate = MutableLiveData<String>()

    fun setItems(items: List<EventEntity>) {
        events.value = items
    }

    fun setDate(date: String) {
        currentDate.value = date
    }

    fun getItem(id: Long) = events.value?.find { it.id == id }
}

class EventViewModelFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(EventViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return EventViewModel() as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}