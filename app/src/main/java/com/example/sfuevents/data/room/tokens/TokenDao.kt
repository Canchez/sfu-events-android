package com.example.sfuevents.data.room.tokens

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import kotlinx.coroutines.flow.Flow

/**
 * DAO for tokens
 */
@Dao
interface TokenDao {
    /**
     * Get all tokens from database
     */
    @Query("SELECT * FROM tokens")
    fun getTokens(): Flow<List<TokenEntity>>

    @Query("SELECT * FROM tokens")
    suspend fun getTokenList(): List<TokenEntity>

    /**
     * Delete all tokens
     */
    @Query("DELETE FROM tokens")
    suspend fun deleteAllTokens()

    /**
     * Insert token
     */
    @Insert
    suspend fun addToken(tokenEntity: TokenEntity)

    /**
     * Update token
     */
    @Update
    suspend fun updateToken(tokenEntity: TokenEntity)
}