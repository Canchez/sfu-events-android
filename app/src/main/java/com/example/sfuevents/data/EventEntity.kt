package com.example.sfuevents.data

data class EventEntity(
    val id: Long = 0,
    val ownerUsername: String = "",
    var post_id: Long = 0,
    var title: String = "",
    var info: String = "",
    var date: String = "",
    var organizationName: String = "",
    var location: String = "",
    var review: String = "",
    var imageUrl: String = "",
)