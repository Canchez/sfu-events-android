package com.example.sfuevents.data.room.tokens

import androidx.annotation.WorkerThread
import kotlinx.coroutines.flow.Flow

/**
 * Repository for tokens
 */
class TokenRepository(private val tokenDao: TokenDao) {

    /**
     * Flow of tokens
     */
    val tokens: Flow<List<TokenEntity>> = tokenDao.getTokens()

    @WorkerThread
    suspend fun getTokenList(): List<TokenEntity> = tokenDao.getTokenList()

    /**
     * Insert new [token]
     */
    @WorkerThread
    suspend fun insert(token: TokenEntity) = tokenDao.addToken(token)

    @WorkerThread
    suspend fun deleteAll() = tokenDao.deleteAllTokens()

    @WorkerThread
    suspend fun update(token: TokenEntity) = tokenDao.updateToken(token)
}