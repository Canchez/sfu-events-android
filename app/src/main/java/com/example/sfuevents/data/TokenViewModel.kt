package com.example.sfuevents.data

import androidx.lifecycle.*
import com.example.sfuevents.data.room.tokens.TokenEntity
import com.example.sfuevents.data.room.tokens.TokenRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class TokenViewModel(private val repository: TokenRepository) : ViewModel() {
    /**
     * Tokens
     */
    val tokens: LiveData<List<TokenEntity>> = repository.tokens.asLiveData()

    fun getFirstAsync() = viewModelScope.async(Dispatchers.IO) {
        repository.getTokenList().firstOrNull()
    }

    /**
     * Insert new [token]
     */
    fun insertToken(token: TokenEntity) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(token)
    }

    /**
     * Delete all tokens
     */
    fun deleteAll() = viewModelScope.launch(Dispatchers.IO) {
        repository.deleteAll()
    }

    fun update(token: TokenEntity) = viewModelScope.launch(Dispatchers.IO) {
        repository.update(token)
    }
}

class TokenViewModelFactory(private val repository: TokenRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TokenViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return TokenViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}