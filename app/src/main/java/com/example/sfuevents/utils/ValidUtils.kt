package com.example.sfuevents.utils

import android.util.Patterns
import java.util.regex.Pattern

class ValidUtils {
    enum class ValidStatuses(val statusMessage: String, val isValid: Boolean = false) {
        VALID("Valid", isValid = true),
        EMAIL_INVALID("Email is invalid"),
        PASSWORD_TOO_SHORT("Password must have $MIN_PASSWORD_LENGTH symbols or more"),
        PASSWORD_TOO_LONG("Password must be less than $MAX_PASSWORD_LENGTH symbols long"),
        PASSWORD_INVALID(PASSWORD_REQUIREMENTS),
        USERNAME_TOO_SHORT("Username must have $MIN_USERNAME_LENGTH symbols or more"),
        USERNAME_TOO_LONG("Username must be less than $MAX_USERNAME_LENGTH symbols long"),

    }

    companion object {
        const val PASSWORD_REQUIREMENTS =
            "Password must contain at least one number, one letter and one special symbol"
        const val MIN_PASSWORD_LENGTH = 8
        const val MAX_PASSWORD_LENGTH = 40
        const val MIN_USERNAME_LENGTH = 3
        const val MAX_USERNAME_LENGTH = 20
        private const val DIGIT_REGEX = "(?=.*[0-9])"
        private const val LOWERCASE_LETTER_REGEX = "(?=.*[a-z])"
        private const val UPPERCASE_LETTER_REGEX = "(?=.*[A-Z])"
        private const val SPECIAL_REGEX = "(?=.*[@#\$%^&+=_])"
        private const val NO_SPACES_REGEX = "(?=\\S+\$)"
        private const val PASSWORD_REGEX =
            "^$DIGIT_REGEX$LOWERCASE_LETTER_REGEX$UPPERCASE_LETTER_REGEX$SPECIAL_REGEX$NO_SPACES_REGEX.{$MIN_PASSWORD_LENGTH,$MAX_PASSWORD_LENGTH}$"


        fun isEmailValid(email: String): ValidStatuses {
            val trimmedEmail = email.trim()
            return if (trimmedEmail.isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(trimmedEmail)
                    .matches()
            ) ValidStatuses.VALID else ValidStatuses.EMAIL_INVALID
        }

        fun isPasswordValid(password: String): ValidStatuses {
            val passwordPattern = Pattern.compile(PASSWORD_REGEX)
            val trimmedPassword = password.trim()
            return when {
                trimmedPassword.length < MIN_PASSWORD_LENGTH -> ValidStatuses.PASSWORD_TOO_SHORT
                trimmedPassword.length > MAX_PASSWORD_LENGTH -> ValidStatuses.PASSWORD_TOO_LONG
                !passwordPattern.matcher(trimmedPassword).matches() -> ValidStatuses.PASSWORD_INVALID
                else -> ValidStatuses.VALID
            }
        }

        fun isUsernameValid(username: String): ValidStatuses {
            val trimmedUsername = username.trim()
            return when {
                trimmedUsername.length < MIN_USERNAME_LENGTH -> ValidStatuses.USERNAME_TOO_SHORT
                trimmedUsername.length > MAX_USERNAME_LENGTH -> ValidStatuses.USERNAME_TOO_LONG
                else -> ValidStatuses.VALID
            }
        }
    }
}