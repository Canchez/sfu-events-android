package com.example.sfuevents.utils

import android.content.Context
import android.widget.Toast
import java.time.LocalDate
import java.time.format.DateTimeFormatter

object HelperUtils {
    fun showToastWithMessage(context: Context, message: String, length: Int = Toast.LENGTH_SHORT) {
        Toast.makeText(context, message, length).show()
    }

    fun formatDateToReadableString(dateTime: String): String {
        val date = dateTime.subSequence(0, 10)
        val time = LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd"))
        return time.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"))
    }

    fun formatDate(localDate: LocalDate): String =
        localDate.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"))
}