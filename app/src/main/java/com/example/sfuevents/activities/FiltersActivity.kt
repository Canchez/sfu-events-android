package com.example.sfuevents.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.example.sfuevents.R
import com.example.sfuevents.databinding.ActivityFiltersBinding

class FiltersActivity : AppCompatActivity() {

    private lateinit var filtersBinding: ActivityFiltersBinding
    private val chosenActivities = mutableListOf<String>()
    private val chosenOrgs = mutableListOf<String>()
    private val chosenInsts = mutableListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        filtersBinding = ActivityFiltersBinding.inflate(layoutInflater)
        setContentView(filtersBinding.root)

        filtersBinding.tvActivity.setOnClickListener {
            filtersBinding.llActivity.isVisible = !filtersBinding.llActivity.isVisible
        }
        filtersBinding.tvDorm.setOnClickListener {
            filtersBinding.llDorm.isVisible = !filtersBinding.llDorm.isVisible
        }
        filtersBinding.tvOrg.setOnClickListener {
            filtersBinding.llOrg.isVisible = !filtersBinding.llOrg.isVisible
        }
        filtersBinding.tvInst.setOnClickListener {
            filtersBinding.llInst.isVisible = !filtersBinding.llInst.isVisible
        }

        fillFilters()
    }

    private fun fillFilters() {
        fillActivities()
        fillDorms()
        fillOrgs()
        fillInsts()
    }

    private fun fillActivities() {
        ACTIVITY_LIST.forEach { inst ->
            val check = createCheckBox("activities", inst)
            filtersBinding.llActivity.addView(check)
        }
    }

    private fun fillDorms() {
        DORM_LIST.forEach { dorm ->
            val check = createCheckBox("organizations", dorm)
            filtersBinding.llDorm.addView(check)
        }
    }

    private fun fillOrgs() {
        ORG_LIST.forEach { org ->
            val check = createCheckBox("organizations", org)
            filtersBinding.llOrg.addView(check)
        }
    }

    private fun fillInsts() {
        INST_LIST.forEach { inst ->
            val check = createCheckBox("institutions", inst)
            filtersBinding.llInst.addView(check)
        }
    }

    private fun createCheckBox(tagText: String, title: String) = CheckBox(this).apply {
        layoutParams = ViewGroup.MarginLayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        ).apply {
            bottomMargin = resources.getDimensionPixelOffset(R.dimen.filter_checkbox_margin)
            topMargin = resources.getDimensionPixelOffset(R.dimen.filter_checkbox_margin)
        }
        text = title
        tag = tagText
        setOnClickListener { checkbox -> onCheckBoxClicked(checkbox) }
    }

    private fun onCheckBoxClicked(view: View) {
        if (view is CheckBox) {
            val checked = view.isChecked
            val textValue = view.text.toString()
            if (checked) {
                when (view.tag) {
                    "activities" -> {
                        if (!chosenActivities.contains(textValue)) {
                            chosenActivities.add(textValue)
                        }
                    }
                    "organizations" -> {
                        if (!chosenOrgs.contains(textValue)) {
                            chosenOrgs.add(textValue)
                        }
                    }
                    "institutions" -> {
                        if (!chosenInsts.contains(textValue)) {
                            chosenInsts.add(textValue)
                        }
                    }
                }
            } else {
                when (view.tag) {
                    "activities" -> {
                        if (chosenActivities.contains(textValue)) {
                            chosenActivities.remove(textValue)
                        }
                    }
                    "organizations" -> {
                        if (chosenOrgs.contains(textValue)) {
                            chosenOrgs.remove(textValue)
                        }
                    }
                    "institutions" -> {
                        if (chosenInsts.contains(textValue)) {
                            chosenInsts.remove(textValue)
                        }
                    }
                }
            }
        }
    }

    override fun onBackPressed() {
        if (chosenActivities.isNotEmpty() || chosenInsts.isNotEmpty() || chosenOrgs.isNotEmpty()) {
            val returnIntent = Intent()
            returnIntent.putExtra(CHOSEN_ACTIVITIES, ArrayList<String>(chosenActivities))
            returnIntent.putExtra(CHOSEN_ORGS, ArrayList<String>(chosenOrgs))
            returnIntent.putExtra(CHOSEN_INSTS, ArrayList<String>(chosenInsts))
            setResult(RESULT_OK, returnIntent)
        } else {
            setResult(RESULT_CANCELED)
        }
        finish()
    }

    companion object {
        const val CHOSEN_ACTIVITIES = "chosenActivities"
        const val CHOSEN_ORGS = "chosenOrgs"
        const val CHOSEN_INSTS = "chosenInsts"

        val ACTIVITY_LIST = arrayOf(
            "учебная",
            "спортивная",
            "научно-исследовательская",
            "общественная",
        )
        val DORM_LIST = arrayOf(
            "Общежитие СФУ №5",
            "Общежитие СФУ №6",
            "Общежитие СФУ №21 (Tesla Village)",
            "Общежитие СФУ №22",
            "Общежитие СФУ №30",
        )
        val ORG_LIST = arrayOf(
            "Microsoft IT Academy ИКИТ",
            "Абитуриент ИКИТ СФУ",
            "Антикоррупционный студенческий клуб",
            "Беги за мной! СФУ",
            "вОбщежитии СФУ",
            "Вокально-инструментальный ансамбль СФУ ПИ 'Интервал'",
            "Волонтерский центр СФУ",
            "Информер ИКИТ СФУ",
            "Информер ПИ СФУ",
            "Клуб интеллектуальных игр СФУ",
            "Корейский язык в СФУ",
            "МЦ ИКИТ СФУ",
            "Научная библиотека СФУ",
            "Научный клуб МЦ ИКИТ",
            "Новости СФУ",
            "НОЦ Молодых ученых СФУ",
            "ПОС ИКИТ (профсоюз)",
            "ПОС ПИ СФУ",
            "ППОС СФУ (Профком студентов)",
            "Психологическая служба СФУ",
            "Рок-клуб СФУ",
            "Сибирский федеральный университет",
            "Совет обучающихся СФУ",
            "Союз молодежи СФУ",
            "Студенческие отряды СФУ",
            "Студенческий комитет",
            "Учебно-организационный отдел ИКИТ",
            "Французский центр СФУ",
            "Центр карьеры СФУ",
            "Центр студенческой культуры СФУ",
            "Японский центр СФУ",
        )
        val INST_LIST = arrayOf(
            "ИКИТ",
            "ПИ",
            "СФУ",
        )
    }
}