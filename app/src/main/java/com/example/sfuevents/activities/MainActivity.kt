package com.example.sfuevents.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.sfuevents.R
import com.example.sfuevents.databinding.ActivityBaseWithBottomNavBarBinding
import com.example.sfuevents.fragments.CalendarFragment
import com.example.sfuevents.fragments.HomeFragment

class MainActivity : AppCompatActivity() {
    private lateinit var mainBinding: ActivityBaseWithBottomNavBarBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBinding = ActivityBaseWithBottomNavBarBinding.inflate(layoutInflater)
        setContentView(mainBinding.root)
        setSupportActionBar(mainBinding.mainToolbar)

        supportFragmentManager.beginTransaction().replace(R.id.mainFragmentContainer, HomeFragment()).commit()
        mainBinding.bottomNavBar.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.homePageItem -> {
                    supportFragmentManager.beginTransaction().replace(R.id.mainFragmentContainer, HomeFragment()).commit()
                    true
                }
                R.id.calendarPageItem -> {
                    supportFragmentManager.beginTransaction().replace(R.id.mainFragmentContainer, CalendarFragment()).commit()
                    true
                }
                else -> {
                    false
                }
            }
        }
    }
}