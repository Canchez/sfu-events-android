package com.example.sfuevents.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.sfuevents.EventsApplication
import com.example.sfuevents.R
import com.example.sfuevents.api.AccessInfo
import com.example.sfuevents.api.ApiUrls
import com.example.sfuevents.api.RefreshInfo
import com.example.sfuevents.data.TokenViewModel
import com.example.sfuevents.data.TokenViewModelFactory
import com.example.sfuevents.data.room.tokens.TokenEntity
import com.example.sfuevents.databinding.ActivityLoadingBinding
import com.example.sfuevents.fragments.LoginPageFragment
import com.squareup.moshi.Moshi
import kotlinx.coroutines.runBlocking
import org.json.JSONObject

class LoadingActivity : AppCompatActivity() {

    private lateinit var loadingBinding: ActivityLoadingBinding

    private lateinit var tokenViewModel: TokenViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        tokenViewModel = TokenViewModelFactory((application as EventsApplication).tokenRepository).create(TokenViewModel::class.java)
        loadingBinding = ActivityLoadingBinding.inflate(layoutInflater)
        setContentView(loadingBinding.root)

        loadingBinding.button.setOnClickListener {
            sendRefresh()
        }
    }

    private fun sendRefresh() {
        loadingBinding.button.isEnabled = false
        loadingBinding.button.text = resources.getString(R.string.title_loading)
        val tokenEntity = runBlocking { tokenViewModel.getFirstAsync().await() }
        Log.d(TAG, "token=$tokenEntity")
        if (tokenEntity != null) {
            val refreshInfo = RefreshInfo(tokenEntity.refreshToken)

            val refreshAdapter = Moshi.Builder().build().adapter(RefreshInfo::class.java)
            val refreshJson = refreshAdapter.toJson(refreshInfo)
            Log.d(TAG, "Sending $refreshJson")

            val queue = Volley.newRequestQueue(this)

            val refreshRequest = JsonObjectRequest(
                Request.Method.POST,
                ApiUrls.REFRESH_URL,
                JSONObject(refreshJson),
                { handleRefreshResponse(it, tokenEntity) },
                { handleRefreshError(it) }
            )

            queue.add(refreshRequest)
        } else {
            startLoginActivity()
        }
    }

    private fun handleRefreshResponse(response: JSONObject, curToken: TokenEntity) {
        Log.d(LoginPageFragment.TAG, response.toString())
        val accessAdapter = Moshi.Builder().build().adapter(AccessInfo::class.java)
        val accessObject = accessAdapter.fromJson(response.toString())

        if (accessObject != null) {
            curToken.token = accessObject.token
            tokenViewModel.update(curToken)
            startMainActivity()
        } else {
            startLoginActivity()
        }
    }

    private fun handleRefreshError(error: VolleyError) {
        loadingBinding.button.isEnabled = true
        loadingBinding.button.text = resources.getString(R.string.button_enter)
        Log.d(LoginPageFragment.TAG, "error=$error")
        startLoginActivity()
    }

    private fun startLoginActivity() {
        val loginIntent = Intent(this, LoginActivity::class.java)
        loginIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(loginIntent)
    }

    private fun startMainActivity() {
        val mainIntent = Intent(this, MainActivity::class.java)
        mainIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(mainIntent)
    }

    companion object {
        const val TAG = "LoadingActivity"
    }
}