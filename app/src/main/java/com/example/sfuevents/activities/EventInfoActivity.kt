package com.example.sfuevents.activities

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.example.sfuevents.EventsApplication
import com.example.sfuevents.R
import com.example.sfuevents.api.ApiUrls
import com.example.sfuevents.data.TokenViewModel
import com.example.sfuevents.data.TokenViewModelFactory
import com.example.sfuevents.databinding.ActivityEventInfoBinding
import com.example.sfuevents.fragments.HomeFragment
import com.example.sfuevents.utils.HelperUtils
import kotlinx.coroutines.runBlocking
import org.json.JSONObject

class EventInfoActivity : AppCompatActivity() {

    private lateinit var eventBinding: ActivityEventInfoBinding

    private lateinit var tokenViewModel: TokenViewModel
    private var eventId = 0L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        eventBinding = ActivityEventInfoBinding.inflate(layoutInflater)
        setContentView(eventBinding.root)
        tokenViewModel =
            TokenViewModelFactory((application as EventsApplication).tokenRepository).create(
                TokenViewModel::class.java
            )

        val extras = intent.extras
        if (extras != null) {
            eventId = extras.getLong(HomeFragment.EVENT_ID, 0L)
            eventBinding.tvEventTitle.text =
                extras.getString(HomeFragment.EVENT_TITLE, "Default Title")
            eventBinding.tvEventDate.text = extras.getString(HomeFragment.EVENT_DATE, "12-34-5678")
            eventBinding.tvEventOrg.text = extras.getString(HomeFragment.EVENT_ORG, "Default Org")
            eventBinding.tvEventInfo.text =
                extras.getString(HomeFragment.EVENT_INFO, "Default Info")
            val imageUrl = extras.getString(HomeFragment.EVENT_IMAGE_URL, "")
            Glide.with(this)
                .load(imageUrl)
                .error(R.drawable.ic_no_image)
                .placeholder(R.drawable.ic_image_loading)
                .into(eventBinding.ivEventImage)
        }
    }

    override fun onStart() {
        super.onStart()
        sendGetEventRequest(eventId)
    }

    private fun sendGetEventRequest(id: Long) {
        eventBinding.btnSaveEvent.isEnabled = false
        eventBinding.btnSaveEvent.text = resources.getString(R.string.title_loading)
        val queue = Volley.newRequestQueue(applicationContext)

        val tokenEntity = runBlocking { tokenViewModel.getFirstAsync().await() }
        Log.d(TAG, "token=${tokenEntity}")

        if (tokenEntity != null) {
            val customRequest = object : JsonObjectRequest(
                Method.GET,
                "${ApiUrls.CALENDAR_URL}${id}/",
                null,
                { handleEventResponse(it) },
                { handleEventError(it) }
            ) {
                override fun getHeaders(): Map<String, String> {
                    return mapOf(
                        "Authorization" to "Bearer ${tokenEntity.token}"
                    )
                }
            }

            queue.add(customRequest)
        } else {
            HelperUtils.showToastWithMessage(this, "Error with token!")
            eventBinding.btnSaveEvent.isEnabled = true
            eventBinding.btnSaveEvent.text = resources.getString(R.string.button_save_event)
            eventBinding.btnSaveEvent.setOnClickListener { sendAddEvent(eventId) }
        }
    }

    private fun handleEventResponse(response: JSONObject) {
        Log.d(TAG, response.toString())
        eventBinding.btnSaveEvent.isEnabled = true
        eventBinding.btnSaveEvent.text = resources.getString(R.string.button_event_saved)
        eventBinding.btnSaveEvent.setOnClickListener { sendDeleteEvent(eventId) }
    }

    private fun handleEventError(error: VolleyError) {
        Log.d(TAG, error.toString())
        eventBinding.btnSaveEvent.isEnabled = true
        eventBinding.btnSaveEvent.text = resources.getString(R.string.button_save_event)
        eventBinding.btnSaveEvent.setOnClickListener { sendAddEvent(eventId) }
    }

    private fun sendAddEvent(id: Long) {
        eventBinding.btnSaveEvent.isEnabled = false
        eventBinding.btnSaveEvent.text = resources.getString(R.string.title_loading)
        val queue = Volley.newRequestQueue(this)

        val tokenEntity = runBlocking { tokenViewModel.getFirstAsync().await() }
        Log.d(TAG, "token=${tokenEntity}")

        if (tokenEntity != null) {
            val customRequest = object : JsonObjectRequest(
                Method.PATCH,
                "${ApiUrls.EVENTS_URL}${id}/join/",
                null,
                { handleEventResponse(it) },
                { handleEventError(it) }
            ) {
                override fun getHeaders(): Map<String, String> {
                    return mapOf(
                        "Authorization" to "Bearer ${tokenEntity.token}"
                    )
                }
            }

            queue.add(customRequest)
        } else {
            HelperUtils.showToastWithMessage(this, "Error with token!")
            eventBinding.btnSaveEvent.isEnabled = true
            eventBinding.btnSaveEvent.text = resources.getString(R.string.button_save_event)
        }
    }

    private fun sendDeleteEvent(id: Long) {
        eventBinding.btnSaveEvent.isEnabled = false
        eventBinding.btnSaveEvent.text = resources.getString(R.string.title_loading)
        val queue = Volley.newRequestQueue(this)

        val tokenEntity = runBlocking { tokenViewModel.getFirstAsync().await() }
        Log.d(TAG, "token=${tokenEntity}")

        if (tokenEntity != null) {
            val customRequest = object : JsonObjectRequest(
                Method.DELETE,
                "${ApiUrls.CALENDAR_URL}${id}/",
                null,
                { handleDeleteResponse(it) },
                { handleDeleteError(it) }
            ) {
                override fun getHeaders(): Map<String, String> {
                    return mapOf(
                        "Authorization" to "Bearer ${tokenEntity.token}"
                    )
                }
            }

            queue.add(customRequest)
        } else {
            HelperUtils.showToastWithMessage(this, "Error with token!")
            eventBinding.btnSaveEvent.isEnabled = true
            eventBinding.btnSaveEvent.text = resources.getString(R.string.button_event_saved)
        }
    }

    private fun handleDeleteResponse(response: JSONObject) {
        Log.d(TAG, response.toString())
        eventBinding.btnSaveEvent.isEnabled = true
        eventBinding.btnSaveEvent.text = resources.getString(R.string.button_save_event)
        eventBinding.btnSaveEvent.setOnClickListener { sendAddEvent(eventId) }
    }

    private fun handleDeleteError(error: VolleyError) {
        Log.d(TAG, error.toString())
        eventBinding.btnSaveEvent.isEnabled = true
        eventBinding.btnSaveEvent.text = resources.getString(R.string.button_event_saved)
        eventBinding.btnSaveEvent.setOnClickListener { sendDeleteEvent(eventId) }
    }

    companion object {
        const val TAG = "EventInfoActivity"
    }
}