package com.example.sfuevents.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.sfuevents.R
import com.example.sfuevents.fragments.LoginPageFragment

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)

        supportFragmentManager.beginTransaction().replace(R.id.loginFragmentContainer, LoginPageFragment()).commit()
    }
}