package com.example.sfuevents.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.example.sfuevents.R
import com.example.sfuevents.databinding.FragmentResetPasswordBinding
import com.example.sfuevents.utils.ValidUtils

class ResetPasswordPageFragment : Fragment() {

    private lateinit var resetPasswordBinding: FragmentResetPasswordBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        resetPasswordBinding = FragmentResetPasswordBinding.inflate(inflater, container, false)
        return resetPasswordBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        resetPasswordBinding.btnResetPassword.setOnClickListener {
            handleReset()
        }

        resetPasswordBinding.teEmail.doOnTextChanged { _, _, _, _ -> clearErrors() }
        resetPasswordBinding.tePassword.doOnTextChanged { _, _, _, _ -> clearErrors() }
        resetPasswordBinding.teRepeatPassword.doOnTextChanged { _, _, _, _ -> clearErrors() }
    }

    private fun clearErrors() {
        resetPasswordBinding.tilEmail.error = ""
        resetPasswordBinding.tilPassword.error = ""
        resetPasswordBinding.tilRepeatPassword.error = ""
    }

    private fun handleReset() {
        if (validateFields()) {
            parentFragmentManager.popBackStack()
        }
    }

    private fun validateFields(): Boolean {
        val email = resetPasswordBinding.teEmail.text.toString()
        val password = resetPasswordBinding.tePassword.text.toString()
        val repeatPassword = resetPasswordBinding.teRepeatPassword.text.toString()

        val emailStatus = ValidUtils.isEmailValid(email)
        val passwordStatus = ValidUtils.isPasswordValid(password)
        val repeatPasswordStatus = ValidUtils.isPasswordValid(repeatPassword)

        if (emailStatus != ValidUtils.ValidStatuses.VALID) {
            resetPasswordBinding.tilEmail.error = emailStatus.statusMessage
        }
        if (passwordStatus != ValidUtils.ValidStatuses.VALID) {
            resetPasswordBinding.tilPassword.error = passwordStatus.statusMessage
        }
        if (repeatPasswordStatus != ValidUtils.ValidStatuses.VALID) {
            resetPasswordBinding.tilRepeatPassword.error = repeatPasswordStatus.statusMessage
        }
        if (repeatPassword != password) {
            resetPasswordBinding.tilRepeatPassword.error = requireContext().getString(R.string.validation_passwords_do_not_match)
        }
        return emailStatus.isValid && passwordStatus.isValid && repeatPasswordStatus.isValid && repeatPassword == password
    }

}