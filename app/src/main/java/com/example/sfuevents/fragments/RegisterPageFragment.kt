package com.example.sfuevents.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.android.volley.Request
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.sfuevents.R
import com.example.sfuevents.api.ApiUrls
import com.example.sfuevents.api.RegistrationInfo
import com.example.sfuevents.databinding.FragmentRegistrationBinding
import com.example.sfuevents.utils.HelperUtils
import com.example.sfuevents.utils.ValidUtils
import com.squareup.moshi.Moshi
import org.json.JSONObject

class RegisterPageFragment : Fragment() {

    private lateinit var registrationBinding: FragmentRegistrationBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        registrationBinding = FragmentRegistrationBinding.inflate(inflater, container, false)
        return registrationBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        registrationBinding.btnRegister.setOnClickListener {
            handleRegister()
        }

        registrationBinding.teEmail.doOnTextChanged { _, _, _, _ -> clearErrors() }
        registrationBinding.teUsername.doOnTextChanged { _, _, _, _ -> clearErrors() }
        registrationBinding.tePassword.doOnTextChanged { _, _, _, _ -> clearErrors() }
        registrationBinding.teRepeatPassword.doOnTextChanged { _, _, _, _ -> clearErrors() }
    }

    private fun clearErrors() {
        registrationBinding.tilEmail.error = ""
        registrationBinding.tilUsername.error = ""
        registrationBinding.tilPassword.error = ""
        registrationBinding.tilRepeatPassword.error = ""
    }

    private fun handleRegister() {
        if (validateFields()) {
            val email = registrationBinding.teEmail.text.toString()
            val username = registrationBinding.teUsername.text.toString()
            val password = registrationBinding.tePassword.text.toString()
            sendRegisterRequest(email, username, password)
        }
    }

    private fun sendRegisterRequest(email: String, username: String, password: String) {
        registrationBinding.btnRegister.isEnabled = false
        registrationBinding.btnRegister.text = resources.getString(R.string.title_loading)
        val registerInfo = RegistrationInfo(email, username, password)

        val registerJsonAdapter = Moshi.Builder().build().adapter(RegistrationInfo::class.java)
        val registerJson = registerJsonAdapter.toJson(registerInfo)

        val queue = Volley.newRequestQueue(requireContext())

        val customRequest = JsonObjectRequest(
            Request.Method.POST,
            ApiUrls.REGISTRATION_URL,
            JSONObject(registerJson),
            { handleRegisterResponse(it) },
            { handleRegisterError(it) }
        )

        queue.add(customRequest)
    }

    private fun handleRegisterResponse(response: JSONObject) {
        registrationBinding.btnRegister.isEnabled = true
        registrationBinding.btnRegister.text = resources.getString(R.string.button_register)
        Log.d(TAG, response.toString())
        HelperUtils.showToastWithMessage(
            requireContext(),
            "Registration is complete",
            Toast.LENGTH_LONG
        )
        parentFragmentManager.popBackStackImmediate()
    }

    private fun handleRegisterError(error: VolleyError) {
        registrationBinding.btnRegister.isEnabled = false
        registrationBinding.btnRegister.text = resources.getString(R.string.button_register)
        Log.d(TAG, "error=$error")
    }

    private fun validateFields(): Boolean {
        val email = registrationBinding.teEmail.text.toString()
        val username = registrationBinding.teUsername.text.toString()
        val password = registrationBinding.tePassword.text.toString()
        val repeatPassword = registrationBinding.teRepeatPassword.text.toString()

        val emailStatus = ValidUtils.isEmailValid(email)
        val usernameStatus = ValidUtils.isUsernameValid(username)
        val passwordStatus = ValidUtils.isPasswordValid(password)
        val repeatPasswordStatus = ValidUtils.isPasswordValid(repeatPassword)

        if (emailStatus != ValidUtils.ValidStatuses.VALID) {
            registrationBinding.tilEmail.error = emailStatus.statusMessage
        }
        if (usernameStatus != ValidUtils.ValidStatuses.VALID) {
            registrationBinding.tilUsername.error = usernameStatus.statusMessage
        }
        if (passwordStatus != ValidUtils.ValidStatuses.VALID) {
            registrationBinding.tilPassword.error = passwordStatus.statusMessage
        }
        if (repeatPasswordStatus != ValidUtils.ValidStatuses.VALID) {
            registrationBinding.tilRepeatPassword.error = repeatPasswordStatus.statusMessage
        }
        if (repeatPassword != password) {
            registrationBinding.tilRepeatPassword.error = requireContext().getString(R.string.validation_passwords_do_not_match)
        }
        return emailStatus.isValid && usernameStatus.isValid && passwordStatus.isValid && repeatPasswordStatus.isValid && repeatPassword == password
    }

    companion object {
        const val TAG = "RegisterPageFragment"
    }
}