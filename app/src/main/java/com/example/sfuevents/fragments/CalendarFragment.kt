package com.example.sfuevents.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.Volley
import com.example.sfuevents.EventsApplication
import com.example.sfuevents.R
import com.example.sfuevents.activities.EventInfoActivity
import com.example.sfuevents.api.ApiUrls
import com.example.sfuevents.api.EventsInfoItem
import com.example.sfuevents.data.*
import com.example.sfuevents.databinding.FragmentCalendarBinding
import com.example.sfuevents.recyclers.event.EventRecyclerAdapter
import com.example.sfuevents.recyclers.event.OnEventListener
import com.example.sfuevents.utils.HelperUtils
import com.squareup.moshi.Moshi
import devs.mulham.horizontalcalendar.HorizontalCalendar
import devs.mulham.horizontalcalendar.model.CalendarEvent
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener
import kotlinx.coroutines.runBlocking
import org.json.JSONArray
import java.time.LocalDate
import java.time.YearMonth
import java.util.*

class CalendarFragment : Fragment(), OnEventListener {

    private lateinit var calendarBinding: FragmentCalendarBinding

    private val tokenViewModel: TokenViewModel by viewModels {
        TokenViewModelFactory((requireActivity().application as EventsApplication).tokenRepository)
    }

    private val eventViewModel: EventViewModel by viewModels {
        EventViewModelFactory()
    }

    private lateinit var horizontalCalendar: HorizontalCalendar
    private val calendar: Calendar = Calendar.getInstance()
    private var curMonth = 0
    private var curYear = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        calendarBinding = FragmentCalendarBinding.inflate(inflater, container, false)
        return calendarBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val eventAdapter = EventRecyclerAdapter(layoutInflater, this)
        calendarBinding.rvEventList.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = eventAdapter
        }


        eventViewModel.currentDate.observe(viewLifecycleOwner) {
            it?.let { date ->
                val list = eventViewModel.events.value?.filter { event -> event.date == date }
                if (list.isNullOrEmpty()) {
                    changeViewsVisibility(
                        showLoading = false,
                        showEmpty = true
                    )
                } else {
                    changeViewsVisibility(
                        showLoading = false,
                        showEmpty = false
                    )
                }
                eventAdapter.submitList(list)
            }
        }


        eventViewModel.setDate(HelperUtils.formatDate(LocalDate.now()))

        calendarBinding.btnNextMonth.setOnClickListener {
            curMonth++
            if (curMonth > Calendar.DECEMBER) {
                curYear++
                curMonth = Calendar.JANUARY
            }
            calendar.set(Calendar.YEAR, curYear)
            calendar.set(Calendar.MONTH, curMonth)
            val monthStr = calendar.getDisplayName(
                Calendar.MONTH,
                Calendar.LONG_STANDALONE,
                Locale.getDefault()
            ) ?: ""
            val dateStr = "${monthStr.capitalize(Locale.getDefault())}, $curYear"
            calendarBinding.tvMonth.text = dateStr
            sendEventsRequest()
        }

        calendarBinding.btnPrevMonth.setOnClickListener {
            curMonth--
            if (curMonth < Calendar.JANUARY) {
                curYear--
                curMonth = Calendar.DECEMBER
            }
            calendar.set(Calendar.YEAR, curYear)
            calendar.set(Calendar.MONTH, curMonth)
            val monthStr = calendar.getDisplayName(
                Calendar.MONTH,
                Calendar.LONG_STANDALONE,
                Locale.getDefault()
            ) ?: ""
            val dateStr = "${monthStr.capitalize(Locale.getDefault())}, $curYear"
            calendarBinding.tvMonth.text = dateStr
            sendEventsRequest()
        }

        calendar.time = Date()
        curMonth = calendar[Calendar.MONTH]
        curYear = calendar[Calendar.YEAR]
        val monthStr =
            calendar.getDisplayName(Calendar.MONTH, Calendar.LONG_STANDALONE, Locale.getDefault())
                ?: ""
        val dateStr = "${monthStr.capitalize(Locale.getDefault())}, $curYear"
        calendarBinding.tvMonth.text = dateStr
        eventViewModel.setDate(HelperUtils.formatDate(LocalDate.now()))
        showCalendar()
    }

    override fun onStart() {
        super.onStart()
        sendEventsRequest()
    }

    private fun getFirstAndLastDatesOfMonth(month: Int, year: Int): Pair<LocalDate, LocalDate> {
        val monthObject = YearMonth.of(year, month)
        val firstDay = LocalDate.of(year, month, 1)
        val lastDay = LocalDate.of(year, month, monthObject.lengthOfMonth())
        return Pair(firstDay, lastDay)
    }

    private fun showCalendar() {

        horizontalCalendar = buildHorizontalCalendar()

        horizontalCalendar.calendarListener = object : HorizontalCalendarListener() {
            override fun onDateSelected(date: Calendar?, position: Int) {
                if (date != null) {
                    val day = date[Calendar.DAY_OF_MONTH]
                    val month = date[Calendar.MONTH]
                    val year = date[Calendar.YEAR]
                    val dateStr = HelperUtils.formatDate(LocalDate.of(year, month + 1, day))
                    eventViewModel.setDate(dateStr)
                    Log.d(TAG, "(year-month-day)=($year-${month + 1}-$day), dateStr=$dateStr")
                }
            }
        }
    }

    private fun buildHorizontalCalendar(): HorizontalCalendar {
        val (firstDay, lastDay) = getFirstAndLastDatesOfMonth(curMonth + 1, curYear)
        val eventDates = eventViewModel.events.value?.map { it.date } ?: emptyList()

        Log.d(TAG, "first=$firstDay, last=$lastDay, curMonth=$curMonth, curYear=$curYear")
        Log.d(TAG, "eventDates=$eventDates")

        val accentColor = resources.getColor(R.color.calendar_dark, requireActivity().theme)
        val iconColor = resources.getColor(R.color.calendar_light, requireActivity().theme)
        return HorizontalCalendar.Builder(requireActivity(), R.id.calendarView)
            .datesNumberOnScreen(NUMBER_OF_DATES_ON_SCREEN)
            .configure()
            .showBottomText(false)
            .formatTopText("EEE")
            .selectorColor(accentColor)
            .textColor(iconColor, accentColor)
            .end()
            .defaultSelectedDate(calendar)
            .range(firstDay.toCalendar(), lastDay.toCalendar())
            .addEvents { date ->
                val day = date[Calendar.DAY_OF_MONTH]
                val month = date[Calendar.MONTH]
                val year = date[Calendar.YEAR]
                if (eventDates.contains(
                        HelperUtils.formatDate(
                            LocalDate.of(
                                year,
                                month + 1,
                                day
                            )
                        )
                    )
                ) {
                    listOf(CalendarEvent(accentColor, ""))
                } else {
                    emptyList()
                }
            }
            .build()
    }

    override fun onEventClick(id: Long) {
        val clickedEvent = eventViewModel.getItem(id)
        if (clickedEvent != null) {
            val eventIntent = Intent(context, EventInfoActivity::class.java)
            eventIntent.putExtra(HomeFragment.EVENT_ID, clickedEvent.id)
            eventIntent.putExtra(HomeFragment.EVENT_TITLE, clickedEvent.title)
            eventIntent.putExtra(HomeFragment.EVENT_DATE, clickedEvent.date)
            eventIntent.putExtra(HomeFragment.EVENT_INFO, clickedEvent.info)
            eventIntent.putExtra(HomeFragment.EVENT_ORG, clickedEvent.organizationName)
            eventIntent.putExtra(HomeFragment.EVENT_IMAGE_URL, clickedEvent.imageUrl)
            startActivity(eventIntent)
        }
    }

    private fun sendEventsRequest() {
        changeViewsVisibility(
            showLoading = true,
            showEmpty = false
        )
        val queue = Volley.newRequestQueue(requireContext())
        val tokenEntity = runBlocking { tokenViewModel.getFirstAsync().await() }
        Log.d(EventInfoActivity.TAG, "token=${tokenEntity}")

        val (firstDay, lastDay) = getFirstAndLastDatesOfMonth(curMonth + 1, curYear)


        if (tokenEntity != null) {
            val customRequest = object : JsonArrayRequest(
                Method.GET,
                ApiUrls.CALENDAR_WITH_DATES_URL(
                    HelperUtils.formatDate(firstDay),
                    HelperUtils.formatDate(lastDay)
                ),
                null,
                { handleEventResponse(it) },
                { handleEventError(it) }
            ) {
                override fun getHeaders(): Map<String, String> {
                    return mapOf(
                        "Authorization" to "Bearer ${tokenEntity.token}"
                    )
                }
            }
            queue.add(customRequest)
        } else {
            changeViewsVisibility(
                showLoading = false,
                showEmpty = true
            )
            HelperUtils.showToastWithMessage(requireContext(), "Error with token!")
        }
    }

    private fun handleEventResponse(response: JSONArray) {
        Log.d(TAG, response.toString())
        val events = mutableListOf<EventEntity>()
        val adapter = Moshi.Builder().build().adapter(EventsInfoItem::class.java)
        for (index in 0..response.length()) {
            val obj = response.optJSONObject(index)
            obj?.let { jsonObj ->
                adapter.fromJson(jsonObj.toString())?.let {
                    EventEntity(
                        id = it.id ?: 0L,
                        ownerUsername = it.owner?.username ?: "",
                        post_id = it.postId ?: 0L,
                        title = it.name ?: "",
                        info = it.description ?: "",
                        date = it.date?.let { date ->
                            HelperUtils.formatDateToReadableString(date)
                        } ?: "",
                        organizationName = it.organization?.name ?: "",
                        location = it.location ?: "",
                        review = it.review ?: "",
                        imageUrl = it.image ?: "",
                    )
                }?.let {
                    events.add(it)
                }
            }
        }
        eventViewModel.setItems(events)
        val (firstDay, lastDay) = getFirstAndLastDatesOfMonth(curMonth + 1, curYear)
        Log.d(TAG, "firstDay=$firstDay, lastDay=$lastDay, curMonth=$curMonth, curYear=$curYear")
        horizontalCalendar.setRange(firstDay.toCalendar(), lastDay.toCalendar())
        if (calendar == LocalDate.now().toCalendar()) {
            horizontalCalendar.selectDate(LocalDate.now().toCalendar(), true)
        } else {
            horizontalCalendar.selectDate(firstDay.toCalendar(), true)
        }
        horizontalCalendar.refresh()
    }

    private fun handleEventError(error: VolleyError) {
        Log.d(TAG, "error=$error")
        changeViewsVisibility(
            showLoading = false,
            showEmpty = true
        )
    }

    private fun changeViewsVisibility(
        showLoading: Boolean = false,
        showEmpty: Boolean = false,
    ) {
        calendarBinding.tvLoading.isVisible = showLoading
        calendarBinding.tvEmpty.isVisible = showEmpty
        calendarBinding.rvEventList.isVisible = !(showEmpty || showLoading)
    }

    companion object {
        const val TAG = "CalendarFragment"
        const val NUMBER_OF_DATES_ON_SCREEN = 8
    }
}

/**
 * Convert [LocalDate] to [Calendar]. Subtract 1 because index
 * of first month in [Calendar] - 0, but [LocalDate] - 1.
 */
fun LocalDate.toCalendar(): Calendar = Calendar.getInstance().apply {
    clear()
    set(this@toCalendar.year, this@toCalendar.monthValue - 1, this@toCalendar.dayOfMonth)
}