package com.example.sfuevents.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.android.volley.Request
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.sfuevents.EventsApplication
import com.example.sfuevents.R
import com.example.sfuevents.activities.MainActivity
import com.example.sfuevents.api.ApiUrls
import com.example.sfuevents.api.LoginInfo
import com.example.sfuevents.api.TokenInfo
import com.example.sfuevents.data.TokenViewModel
import com.example.sfuevents.data.TokenViewModelFactory
import com.example.sfuevents.data.room.tokens.TokenEntity
import com.example.sfuevents.databinding.FragmentLoginBinding
import com.example.sfuevents.utils.HelperUtils
import com.example.sfuevents.utils.ValidUtils
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import org.json.JSONObject

class LoginPageFragment : Fragment() {


    lateinit var loginBinding: FragmentLoginBinding

    private val tokenViewModel: TokenViewModel by viewModels {
        TokenViewModelFactory((requireActivity().application as EventsApplication).tokenRepository)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        loginBinding = FragmentLoginBinding.inflate(inflater, container, false)
        return loginBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        loginBinding.btnLogin.setOnClickListener {
            handleLogin()
        }

        loginBinding.btnRegister.setOnClickListener {
            handleRegister()
        }

        loginBinding.btnResetPassword.setOnClickListener {
            handleReset()
        }

        loginBinding.teEmail.doOnTextChanged { _, _, _, _ -> clearErrors() }
        loginBinding.tePassword.doOnTextChanged { _, _, _, _ -> clearErrors() }
    }

    private fun clearErrors() {
        loginBinding.tilEmail.error = ""
        loginBinding.tilPassword.error = ""
    }

    private fun handleLogin() {
        if (validateFields()) {
            val email = loginBinding.teEmail.text.toString()
            val password = loginBinding.tePassword.text.toString()
            sendLoginRequest(email, password)
        }
    }

    private fun handleRegister() {
        navigateToFragment(RegisterPageFragment())
    }

    private fun handleReset() {
        navigateToFragment(ResetPasswordPageFragment())
    }

    private fun startMainActivity() {
        val mainIntent = Intent(context, MainActivity::class.java)
        mainIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(mainIntent)
    }

    private fun navigateToFragment(fragment: Fragment) {
        val transaction = parentFragmentManager.beginTransaction().replace(R.id.loginFragmentContainer, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private fun sendLoginRequest(email: String, password: String) {
        loginBinding.btnLogin.isEnabled = false
        loginBinding.btnLogin.text = resources.getString(R.string.title_loading)
        val loginInfo = LoginInfo(email, password)

        val loginJsonAdapter = Moshi.Builder().build().adapter(LoginInfo::class.java)
        val loginJson = loginJsonAdapter.toJson(loginInfo)
        Log.d(TAG, "Sending $loginJson")

        val queue = Volley.newRequestQueue(requireContext())

        val customRequest = JsonObjectRequest(
            Request.Method.POST,
            "${ApiUrls.LOGIN_URL}/",
            JSONObject(loginJson),
            { handleLoginResponse(it) },
            { handleLoginError(it) }
        )

        queue.add(customRequest)
    }

    private fun handleLoginResponse(response: JSONObject) {
        loginBinding.btnLogin.isEnabled = true
        loginBinding.btnLogin.text = resources.getString(R.string.button_login)
        Log.d(TAG, response.toString())
        val loginResponseAdapter: JsonAdapter<TokenInfo> =
            Moshi.Builder().build().adapter(TokenInfo::class.java)
        val loginResponseObject = loginResponseAdapter.fromJson(response.toString())

        if (loginResponseObject != null) {
            val tokenEntity =
                TokenEntity(loginResponseObject.token, loginResponseObject.refreshToken)
            tokenViewModel.insertToken(tokenEntity)
            Log.d(TAG, "Inserted token ${tokenEntity.token}")
            startMainActivity()
        }
    }

    private fun handleLoginError(error: VolleyError) {
        loginBinding.btnLogin.isEnabled = true
        loginBinding.btnLogin.text = resources.getString(R.string.button_login)
        Log.d(TAG, error.toString())
        HelperUtils.showToastWithMessage(requireContext(), "Login Error, ${error.message}")
    }

    private fun validateFields(): Boolean {
        val email = loginBinding.teEmail.text.toString()
        val password = loginBinding.tePassword.text.toString()

        val emailStatus = ValidUtils.isEmailValid(email)
        val passwordStatus = ValidUtils.isPasswordValid(password)

        if (emailStatus != ValidUtils.ValidStatuses.VALID) {
            loginBinding.tilEmail.error = emailStatus.statusMessage
        }
        if (passwordStatus != ValidUtils.ValidStatuses.VALID) {
            loginBinding.tilPassword.error = passwordStatus.statusMessage
        }
        return emailStatus.isValid && passwordStatus.isValid
    }

    companion object {
        const val TAG = "LoginPageFragment"
    }

}