package com.example.sfuevents.fragments

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.*
import com.android.volley.toolbox.HttpHeaderParser
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonRequest
import com.android.volley.toolbox.Volley
import com.example.sfuevents.EventsApplication
import com.example.sfuevents.R
import com.example.sfuevents.activities.EventInfoActivity
import com.example.sfuevents.activities.FiltersActivity
import com.example.sfuevents.activities.LoginActivity
import com.example.sfuevents.api.ApiUrls
import com.example.sfuevents.api.EventsInfoItem
import com.example.sfuevents.api.FilterInfo
import com.example.sfuevents.data.*
import com.example.sfuevents.databinding.FragmentHomeBinding
import com.example.sfuevents.recyclers.event.EventRecyclerAdapter
import com.example.sfuevents.recyclers.event.OnEventListener
import com.example.sfuevents.utils.HelperUtils
import com.squareup.moshi.Moshi
import org.json.JSONArray
import org.json.JSONException
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset

class HomeFragment : Fragment(), OnEventListener {

    private lateinit var homeBinding: FragmentHomeBinding

    private val tokenViewModel: TokenViewModel by viewModels {
        TokenViewModelFactory((requireActivity().application as EventsApplication).tokenRepository)
    }

    private val eventViewModel: EventViewModel by viewModels {
        EventViewModelFactory()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        homeBinding = FragmentHomeBinding.inflate(inflater, container, false)
        return homeBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)

        val eventAdapter = EventRecyclerAdapter(layoutInflater, this)
        homeBinding.rvEventList.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = eventAdapter
        }

        eventViewModel.events.observe(viewLifecycleOwner) { events ->
            events?.let { eventAdapter.submitList(it) }
        }

        sendEventParseRequest()
    }

    override fun onEventClick(id: Long) {
        val clickedEvent = eventViewModel.getItem(id)
        if (clickedEvent != null) {
            val eventIntent = Intent(context, EventInfoActivity::class.java)
            eventIntent.putExtra(EVENT_ID, clickedEvent.id)
            eventIntent.putExtra(EVENT_TITLE, clickedEvent.title)
            eventIntent.putExtra(EVENT_DATE, clickedEvent.date)
            eventIntent.putExtra(EVENT_INFO, clickedEvent.info)
            eventIntent.putExtra(EVENT_ORG, clickedEvent.organizationName)
            eventIntent.putExtra(EVENT_IMAGE_URL, clickedEvent.imageUrl)
            startActivity(eventIntent)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.home_fragment_options, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menuFilters -> {
                val intent = Intent(requireContext(), FiltersActivity::class.java)
                startActivityForResult(intent, FILTERS_ACTIVITY_REQUEST_CODE)
                true
            }
            R.id.menuLogout -> {
                tokenViewModel.deleteAll()
                val loginIntent = Intent(requireContext(), LoginActivity::class.java)
                loginIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(loginIntent)
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == FILTERS_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK && data != null) {
                val chosenActivities =
                    data.getStringArrayListExtra(FiltersActivity.CHOSEN_ACTIVITIES)
                val chosenOrgs = data.getStringArrayListExtra(FiltersActivity.CHOSEN_ORGS)
                val chosenInsts = data.getStringArrayListExtra(FiltersActivity.CHOSEN_INSTS)

                val activities = chosenActivities?.filterNotNull()
                val orgs = chosenOrgs?.filterNotNull()
                val insts = chosenInsts?.filterNotNull()
                sendFiltersRequest(
                    activities = activities ?: listOf(),
                    insts = insts ?: listOf(),
                    orgs = orgs ?: listOf(),
                )
            }
        }
    }

    private fun sendEventParseRequest() {
        changeViewsVisibility(
            showLoading = true,
            showEmpty = false
        )
        val queue = Volley.newRequestQueue(requireContext())

        val customRequest = JsonArrayRequest(
            Request.Method.GET,
            ApiUrls.EVENTS_URL,
            null,
            { handleParsedEvents(it) },
            { handleParseError(it) }
        )

        queue.add(customRequest)
    }

    private fun handleParsedEvents(response: JSONArray) {
        val events = mutableListOf<EventEntity>()
        val adapter = Moshi.Builder().build().adapter(EventsInfoItem::class.java)
        for (index in 0..response.length()) {
            val obj = response.optJSONObject(index)
            obj?.let { jsonObj ->
                adapter.fromJson(jsonObj.toString())?.let {
                    EventEntity(
                        id = it.id ?: 0L,
                        ownerUsername = it.owner?.username ?: "",
                        post_id = it.postId ?: 0L,
                        title = it.name ?: "",
                        info = it.description ?: "",
                        date = it.date?.let { date ->
                            HelperUtils.formatDateToReadableString(date)
                        } ?: "",
                        organizationName = it.organization?.name ?: "",
                        location = it.location ?: "",
                        review = it.review ?: "",
                        imageUrl = it.image ?: "",
                    )
                }?.let {
                    events.add(it)
                }
            }
        }
        changeViewsVisibility(
            showLoading = false,
            showEmpty = events.isEmpty()
        )
        eventViewModel.setItems(events)
    }

    private fun handleParseError(error: VolleyError) {
        Log.d(TAG, error.toString())
    }

    private fun changeViewsVisibility(
        showLoading: Boolean = false,
        showEmpty: Boolean = false,
    ) {
        homeBinding.tvLoading.isVisible = showLoading
        homeBinding.tvEmpty.isVisible = showEmpty
        homeBinding.rvEventList.isVisible = !(showEmpty || showLoading)
    }

    private fun sendFiltersRequest(
        activities: List<String>,
        insts: List<String>,
        orgs: List<String>
    ) {
        changeViewsVisibility(
            showLoading = true,
            showEmpty = false
        )
        val queue = Volley.newRequestQueue(requireContext())

        val filterInfo = FilterInfo(
            activities = activities,
            institutions = insts,
            organizations = orgs
        )

        val filterJsonAdapter = Moshi.Builder().build().adapter(FilterInfo::class.java)
        val filterJson = filterJsonAdapter.toJson(filterInfo)
        Log.d(TAG, "Sending $filterJson")

        val customRequest = object : JsonRequest<JSONArray>(
            Method.POST,
            ApiUrls.FILTERED_EVENTS_URL,
            filterJson,
            { handleParsedEvents(it) },
            { handleParseError(it) }
        ) {
            override fun parseNetworkResponse(response: NetworkResponse?): Response<JSONArray> {
                try {
                    val jsonString = response?.let {
                        String(
                            it.data,
                            Charset.forName(
                                HttpHeaderParser.parseCharset(
                                    response.headers,
                                    PROTOCOL_CHARSET
                                )
                            )
                        )
                    }
                    return if (jsonString != null) {
                        Response.success(
                            JSONArray(jsonString),
                            HttpHeaderParser.parseCacheHeaders(response)
                        )
                    } else {
                        Response.error(ParseError())
                    }
                } catch (e: UnsupportedEncodingException) {
                    return Response.error(ParseError(e))
                } catch (je: JSONException) {
                    return Response.error(ParseError(je))
                }

            }

        }

        queue.add(customRequest)
    }

    companion object {
        const val TAG = "HomeFragment"

        const val EVENT_ID = "eventId"
        const val EVENT_TITLE = "eventTitle"
        const val EVENT_DATE = "eventDate"
        const val EVENT_INFO = "eventInfo"
        const val EVENT_ORG = "eventOrg"
        const val EVENT_IMAGE_URL = "eventImageUrl"

        const val FILTERS_ACTIVITY_REQUEST_CODE = 1
    }
}