package com.example.sfuevents.recyclers.event

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.sfuevents.R
import com.example.sfuevents.data.EventEntity
import com.example.sfuevents.databinding.ItemNewsItemBinding

class EventViewHolder(
    private val binding: ItemNewsItemBinding,
    private val onEventListener: OnEventListener,
) : RecyclerView.ViewHolder(binding.root), View.OnClickListener {
    fun bindTo(event: EventEntity) {
        binding.tvEventInfo.text = event.info
        binding.btnOpenEventInfo.setOnClickListener(this)
        Glide.with(itemView)
            .load(event.imageUrl)
            .error(R.drawable.ic_no_image)
            .placeholder(R.drawable.ic_image_loading)
            .into(binding.ivEventImage)
        itemView.tag = event.id
    }

    override fun onClick(v: View?) {
        onEventListener.onEventClick(itemView.tag as Long)
    }
}