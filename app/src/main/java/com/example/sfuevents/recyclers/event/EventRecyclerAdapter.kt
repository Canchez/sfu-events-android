package com.example.sfuevents.recyclers.event

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.sfuevents.data.EventEntity
import com.example.sfuevents.databinding.ItemNewsItemBinding

class EventRecyclerAdapter(
    private val inflater: LayoutInflater,
    private val onEventListener: OnEventListener,
) : ListAdapter<EventEntity, EventViewHolder>(EventDiffer) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder {
        return EventViewHolder(
            ItemNewsItemBinding.inflate(inflater, parent, false),
            onEventListener,
        )
    }

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        holder.bindTo(getItem(position))
    }

    private object EventDiffer : DiffUtil.ItemCallback<EventEntity>() {
        override fun areContentsTheSame(oldItem: EventEntity, newItem: EventEntity): Boolean {
            return oldItem == newItem
        }

        override fun areItemsTheSame(oldItem: EventEntity, newItem: EventEntity): Boolean {
            return oldItem.id == newItem.id
        }
    }
}