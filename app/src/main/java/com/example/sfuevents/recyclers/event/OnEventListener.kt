package com.example.sfuevents.recyclers.event

interface OnEventListener {
    fun onEventClick(id: Long)
}