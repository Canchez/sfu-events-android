package com.example.sfuevents

import android.app.Application
import com.example.sfuevents.data.room.tokens.TokenDatabase
import com.example.sfuevents.data.room.tokens.TokenRepository

class EventsApplication : Application() {

    /**
     * Singleton of [TokenDatabase]
     */
    private val database by lazy { TokenDatabase.getDatabase(this) }

    /**
     * Repository of [TokenDatabase]
     */
    val tokenRepository by lazy { TokenRepository(database.tokenDao()) }
}