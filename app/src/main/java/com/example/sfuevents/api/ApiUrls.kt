package com.example.sfuevents.api

object ApiUrls {
    private const val BASE_URL = "https://5c6ddfd7d14f.ngrok.io"
    private const val USERS_BASE_URL = "$BASE_URL/users-api"

    // users-api
    const val LOGIN_URL = "$USERS_BASE_URL/token"
    const val REFRESH_URL = "$LOGIN_URL/refresh/"
    const val REGISTRATION_URL = "$USERS_BASE_URL/registration/"
//    const val PASSWORD_RESET_URL = "$LOGIN_URL/password/reset/"
//    const val PASSWORD_RESET_CONFIRM_URL = "$PASSWORD_RESET_URL/confirm/"

    // events.api
    const val EVENTS_URL = "$BASE_URL/events-api/"
    const val FILTERED_EVENTS_URL = "${EVENTS_URL}get_filtered_events/"
    const val CALENDAR_URL = "$BASE_URL/calendar-api/"
    fun CALENDAR_WITH_DATES_URL(dateGte: String, dateLte: String) =
        "${CALENDAR_URL}?date__gte=${dateGte}T00:00:00+07:00&date__lte=${dateLte}T00:00:00+07:00"
}