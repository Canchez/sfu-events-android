package com.example.sfuevents.api

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class LoginInfo(
    @Json(name = "email")
    val email: String,
    @Json(name = "password")
    val password: String,
)