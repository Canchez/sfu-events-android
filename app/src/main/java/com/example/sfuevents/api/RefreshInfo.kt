package com.example.sfuevents.api

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RefreshInfo(
    @Json(name = "refresh")
    val refreshToken: String,
)