package com.example.sfuevents.api

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TokenInfo(
    @Json(name = "access")
    val token: String,
    @Json(name = "refresh")
    val refreshToken: String,
)