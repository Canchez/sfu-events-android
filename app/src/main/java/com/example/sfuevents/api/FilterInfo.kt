package com.example.sfuevents.api

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FilterInfo(
    @Json(name = "organizations")
    val organizations: List<String> = listOf(),
    @Json(name = "institutions")
    val institutions: List<String> = listOf(),
    @Json(name = "activities")
    val activities: List<String> = listOf(),
)