package com.example.sfuevents.api

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RegistrationInfo(
    @Json(name = "email")
    var email: String,
    @Json(name = "username")
    var username: String,
    @Json(name = "password")
    var password: String,
)