package com.example.sfuevents.api

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AccessInfo(
    @Json(name = "access")
    val token: String,
)