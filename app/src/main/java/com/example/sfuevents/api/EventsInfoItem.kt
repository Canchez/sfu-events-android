package com.example.sfuevents.api


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class EventsInfoItem(
    @Json(name = "date")
    val date: String?,
    @Json(name = "description")
    val description: String?,
    @Json(name = "id")
    val id: Long?,
    @Json(name = "image")
    val image: String?,
    @Json(name = "location")
    val location: String?,
    @Json(name = "name")
    val name: String?,
    @Json(name = "organization")
    val organization: Organization?,
    @Json(name = "owner")
    val owner: Owner?,
    @Json(name = "post_id")
    val postId: Long?,
    @Json(name = "review")
    val review: String?
) {
    @JsonClass(generateAdapter = true)
    data class Organization(
        @Json(name = "name")
        val name: String?
    )

    @JsonClass(generateAdapter = true)
    data class Owner(
        @Json(name = "email")
        val email: String?,
        @Json(name = "username")
        val username: String?
    )
}